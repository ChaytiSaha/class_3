import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.teal.withOpacity(0.9),
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            //crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('assets/images/chayti.jpg'),
          ),
              Text('Chayti Saha', style: TextStyle(
                fontFamily: "Pacifico",
                fontSize: 40,
                color: Colors.white
              ),),
              Text('FLUTTER DEVELOPER', style: TextStyle(
                  fontSize: 20,
                  color: Colors.black26,
                  letterSpacing: 2.5
              ),),
              SizedBox(
                width: 150,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
                  Container(
                    width: 340,
                    height: 50,
                    margin: EdgeInsets.symmetric(vertical: 10,horizontal: 50),
                    padding: EdgeInsets.only(left: 10),
                    decoration: BoxDecoration(
                      border:Border.all(
                        color: Colors.black26,
                        width: 5,
                      ),
                      gradient: LinearGradient(
                        begin: Alignment(-1,-1),
                        end: Alignment(-1,1),
                        colors: [Colors.white,Colors.black12],
                      ),
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Row(
                      children: [
                        Icon(
                            Icons.phone,
                            color: Colors.black,
                        ),
                        SizedBox(
                          width: 15
                        ),
                        Text('+88 01521333333'),
                      ],
                    ),
                  ),
              Container(
                width: 340,
                height: 50,
                margin: EdgeInsets.symmetric(vertical: 10,horizontal: 50),
                padding: EdgeInsets.only(left: 10),
                decoration: BoxDecoration(
                    border:Border.all(
                      color: Colors.black26,
                      width: 5,
                    ),
                    gradient: LinearGradient(
                      begin: Alignment(-1,-1),
                      end: Alignment(-1,1),
                      colors: [Colors.white,Colors.black12],
                    ),
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  children: [
                    Icon(
                      Icons.email,
                      color: Colors.black,
                    ),
                    SizedBox(
                        width: 15
                    ),
                    Text('abcxyz99@gmail.com'),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}


